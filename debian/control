Source: partitionmanager
Section: admin
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.91~),
               gettext,
               libkf5config-dev (>= 5.91~),
               libkf5configwidgets-dev (>= 5.91~),
               libkf5coreaddons-dev (>= 5.91~),
               libkf5crash-dev (>= 5.91~),
               libkf5dbusaddons-dev (>= 5.91~),
               libkf5doctools-dev (>= 5.91~),
               libkf5i18n-dev (>= 5.91~),
               libkf5jobwidgets-dev (>= 5.91~),
               libkf5kio-dev (>= 5.91~),
               libkf5widgetsaddons-dev (>= 5.91~),
               libkf5windowsystem-dev (>= 5.91~),
               libkf5xmlgui-dev (>= 5.91~),
               libkpmcore-dev (>= 21.04~),
               libpolkit-qt5-1-dev,
               pkg-config,
               pkg-kde-tools,
               qtbase5-dev (>= 5.15.2~),
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.kde.org/applications/system/kdepartitionmanager
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/partitionmanager
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/partitionmanager.git

Package: partitionmanager
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Suggests: btrfs-progs,
          dosfstools,
          hfsplus,
          hfsutils,
          jfsutils,
          ntfs-3g,
          reiser4progs,
          reiserfsprogs,
          xfsprogs,
Description: file, disk and partition management for KDE
 Partition Manager is a utility program to help you manage the disk devices,
 partitions and file systems on your computer. It allows you to easily create,
 copy, move, delete, resize without losing data, backup and restore partitions.
 .
 Partition Manager supports a large number of file systems, including ext2/3/4,
 reiserfs, NTFS, FAT16/32, jfs, xfs and more. Note that to gain support for a
 specific file system other than ext2/3/4, you should install the corresponding
 suggested package.
 .
 Partition Manager is based on libparted (like gparted) and makes use of the
 KDE libraries for its user interface.
